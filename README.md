# Dotfiles

These are my Dotfiles for Linux/Unix like systems

This Repository is released under the [MIT License](./LICENSE).

There are however Plugins for tmux and vim that are integrated and have their own Licenses in the Folder.


## Dependencies / Config files included for

* GNU stow
* tmux
	* tmuxp
		* python3 + pip
* git
* btop
* vim
* bash
* Starship
* Ranger


# Setup


## With setup.sh

The `setup.sh` Script is DESTRUCTIVE. Please check the Script before running and backup files

Change the `SCRIPT_LOCATION` Variable to youre prefered Location


## With Stow

With Stow, you will need to delete the files

```bash
mkdir ~/.config # To not create a Symbolic Link to the .config Directory
stow . -t ~/
```
